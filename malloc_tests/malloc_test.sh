#!/bin/zsh

reg='^\./*'

if [[ $0 =~ $reg ]]; then
	MT_Run_path="$( cd "$(dirname "$0")" ; pwd -P )"
else
	MT_Run_path="$( dirname $0 )"
fi

export DYLD_LIBRARY_PATH=$( echo $MT_Run_path | sed 's|\/malloc_tests||g' )
#export MLC_SHOW_CHUNK=
#export MLC_SHOW_IN_COLOR=
#export MLC_SHOW_EMPTY=
export MLC_ECHO=
export MLC_ECHO_TIMESTAMP=
bin_dir="$MT_Run_path/bin"
src_dir="$MT_Run_path/src"
lib="$DYLD_LIBRARY_PATH/libft_malloc.so"
include="-I$DYLD_LIBRARY_PATH/inc"

exec 3>& /dev/stdout
exec 4>& /dev/stderr

for flag in $argv ;do
	case $flag in
	-a)
		all=''
		;;
	-t)
		tiny=''
		;;
	-s)
		small=''
		;;
	-l)
		large=''
		;;
	-e)
		errors=''
		;;
	-r)
		realloc=''
		;;
	-d)
		dup=''
		;;
	-m1)
		exec 3>& /dev/null
		;;
	-m2)
		exec 4>& /dev/null
		;;
	-h | *)
		echo "usage: ./malloc_test.sh [-a -t -s -l -e -r -m1 -m2 -h]"
		exit 0
		;;
	esac
done

mkdir -p $bin_dir || exit 1
make -C $DYLD_LIBRARY_PATH || exit 1
let 'err_num=0'
let 'test_num=0'

function MT_Run {
	echo '\e[2m/-----::\t'$1'\e[0m'
	gcc $include $lib $src_dir/$1.c -o $bin_dir/$1.out
	$bin_dir/$1.out 1>&3 2>&4
	if [[ ? -ne 0 ]]; then
		echo '\e[31mKO\t::-----/\n\e[0m'
		let 'err_num++'
	else
		echo '\e[32mOK\t::-----/\n\e[0m'
	fi
	let 'test_num++'
}
function MT_Summary {
	echo "\e[37;44;4mSUMMARY:\t\
	TESTS = $test_num\t\
	ERRORS = $err_num (5 ok)\e[0m"
	rm -rf $bin_dir
}
function MT_Tiny {
	echo '\e[37;44;4m   TINY ALLOCATIONS   \e[0m'
	mkdir -p $bin_dir/tiny
	MT_Run tiny/t_zero
	MT_Run tiny/t_multiple
	MT_Run tiny/t_many
	MT_Run tiny/t_dealloc_1
	MT_Run tiny/t_dealloc_2
	MT_Run tiny/t_dealloc_3
}
function MT_Small {
	echo '\e[37;44;4m   SMALL ALLOCATIONS   \e[0m'
	mkdir -p $bin_dir/small
	MT_Run small/s_multiple
	MT_Run small/s_many
	MT_Run small/s_dealloc_1
	MT_Run small/s_dealloc_2
	MT_Run small/s_dealloc_3
}
function MT_Large {
	echo '\e[37;44;4m   LARGE ALLOCATIONS   \e[0m'
	mkdir -p $bin_dir/large
	MT_Run large/l_multiple
	MT_Run large/l_many
	MT_Run large/l_dealloc
}
function MT_Errors {
	echo '\e[37;44;4m   ERRORS   \e[0m'
	mkdir -p $bin_dir/errors
	MT_Run errors/double_free
	MT_Run errors/invalid_pointer_1
	MT_Run errors/invalid_pointer_2
	MT_Run errors/invalid_pointer_3
	MT_Run errors/invalid_pointer_4
}
function MT_Realloc {
	echo '\e[37;44;4m   REALLOC   \e[0m'
	mkdir -p $bin_dir/realloc
	MT_Run realloc/realloc_1
	MT_Run realloc/realloc_2
	MT_Run realloc/realloc_3
	MT_Run realloc/realloc_4
	MT_Run realloc/realloc_5
	MT_Run realloc/realloc_6
	MT_Run realloc/realloc_7
	MT_Run realloc/realloc_8
	MT_Run realloc/realloc_9
	MT_Run realloc/realloc_10
	MT_Run realloc/realloc_11
	MT_Run realloc/realloc_12
	MT_Run realloc/realloc_13
}
function MT_Dupl {
	echo '\e[37;44;4m   DUPLICATING   \e[0m'
	mkdir -p $bin_dir/duplicates
	MT_Run duplicates/dup_l2l
	MT_Run duplicates/dup_l2s
	MT_Run duplicates/dup_l2t
	MT_Run duplicates/dup_s2l
	MT_Run duplicates/dup_s2s
	MT_Run duplicates/dup_s2t
	MT_Run duplicates/dup_t2l
	MT_Run duplicates/dup_t2s
	MT_Run duplicates/dup_t2t
}
if [[ -v all || (! -v tiny && ! -v small && ! -v large && ! -v errors\
	&& ! -v realloc && ! -v dup) ]]; then
	MT_Tiny
	MT_Small
	MT_Large
	MT_Errors
	MT_Realloc
	MT_Dupl
	MT_Summary
	exit 0
fi
if [[ -v tiny ]]; then; MT_Tiny; fi
if [[ -v small ]]; then; MT_Small; fi
if [[ -v large ]]; then; MT_Large; fi
if [[ -v errors ]]; then; MT_Errors; fi
if [[ -v realloc ]]; then; MT_Realloc; fi
if [[ -v dup ]]; then; MT_Dupl; fi
MT_Summary
