#include "ft_malloc.h"

int main(void)
{
	char *b, *c, *x;

	x = malloc(4);
	x = malloc(4);
	c = malloc(4);
	b = malloc(4);
	show_alloc_mem();
	free(b);
	free(c);
	show_alloc_mem();
	c = malloc(24);
	show_alloc_mem();
	return 0;
}
