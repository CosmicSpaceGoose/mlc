#include "ft_malloc.h"

/*
 * merge with part of right and left
 */

int main()
{
	void *in_use, *left, *right, *new_ptr, *ptr;

	left = malloc(22);
	ptr = malloc(22);
	right = malloc(16);
	new_ptr = malloc(16);
	in_use = malloc(0);
	show_alloc_mem();
	free(left);
	free(right);
	show_alloc_mem();
	free(ptr);
	show_alloc_mem();
	free(new_ptr);
	show_alloc_mem();
	return (0);
}
