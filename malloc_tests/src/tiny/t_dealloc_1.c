#include "ft_malloc.h"

int main(void)
{
	char *b, *c, *x;

	x = malloc(16);
	c = malloc(4);
	b = malloc(23);
	x = malloc(44);
	show_alloc_mem();
	free(b);
	free(c);
	show_alloc_mem();
	c = malloc(4);
	show_alloc_mem();
	return 0;
}
