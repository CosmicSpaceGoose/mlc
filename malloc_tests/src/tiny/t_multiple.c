#include "ft_malloc.h"

int	main(void)
{
	void	*a;
	int		*b;
	size_t	*c;

	a = malloc(8);
	b = (int *)malloc(sizeof(int) * 2);
	c = (size_t *)malloc(sizeof(size_t) * 1);
	show_alloc_mem();
	return (0);
}
