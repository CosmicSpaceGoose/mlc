#include "ft_malloc.h"

int	main(void)
{
	void	*a;

	a = malloc(8);
	show_alloc_mem();
	free(a);
	show_alloc_mem();
	free(a);
	return (0);
}
