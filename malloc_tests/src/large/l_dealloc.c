#include "ft_malloc.h"

int main(void)
{
	void *b, *x;

	x = malloc(8000);
	b = malloc(34567);
	x = malloc(6762);
	free(b);
	show_alloc_mem();
	b = malloc(34567);
	show_alloc_mem();
	return (0);
}
