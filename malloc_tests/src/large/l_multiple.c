#include "ft_malloc.h"

int	main(void)
{
	void	*a;
	int		*b;
	size_t	*c;

	a = malloc(8160);
	b = (int *)malloc(sizeof(int) * 8160);
	c = (size_t *)malloc(sizeof(size_t) * 8160);
	show_alloc_mem();
	return (0);
}
