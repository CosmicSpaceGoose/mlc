#include "ft_malloc.h"
#include <stdio.h>

/*
 * merge with left and right
 */

int main()
{
	void *prev, *in_use, *left, *right, *new_ptr, *ptr;

	prev = malloc(0);
	in_use = malloc(0);
	left = malloc(8);
	ptr = malloc(8);
	right = malloc(8);
	in_use = malloc(0);
	show_alloc_mem();
	free(prev);
	free(left);
	free(right);
	show_alloc_mem();
	new_ptr = realloc(ptr, 40);
	printf("realloc %p 8b to %p 40b\n", ptr, new_ptr);
	show_alloc_mem();
	in_use = malloc(0);
	in_use = malloc(0);
	in_use = malloc(0);
	show_alloc_mem();
	return (0);
}
