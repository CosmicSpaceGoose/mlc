#include "ft_malloc.h"

/*
 * decrease then increase (duplicate) allocated chunk
 */

int main(void)
{
	void *ptr, *new_ptr, *in_use;

	ptr = malloc(32);
	in_use = malloc(8);
	show_alloc_mem();
	new_ptr = realloc(ptr, 8);
	show_alloc_mem();
	ptr = realloc(new_ptr, 24);
	in_use = malloc(8);
	in_use = malloc(16);
	show_alloc_mem();
	free(ptr);
	return (0);
}
