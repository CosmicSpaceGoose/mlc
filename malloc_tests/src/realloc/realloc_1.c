#include "ft_malloc.h"
#include <stdio.h>
/*
 * decrease then increase (duplicate) allocated chunk
 */

int main(void)
{
	void *ptr, *new_ptr;

	ptr = malloc(5000);
	show_alloc_mem();
	new_ptr = realloc(ptr, 7000);
	show_alloc_mem();
	printf("%p:%p\n", ptr, new_ptr);
	return (0);
}
