#include "ft_malloc.h"

/*
 * decrease allocated chunk
 */

int main(void)
{
	void *ptr, *new_ptr, *in_use;

	ptr = malloc(32);
	new_ptr = malloc(8);
	in_use = malloc(8);
	show_alloc_mem();
	free(new_ptr);
	new_ptr = realloc(ptr, 16);
	show_alloc_mem();
	ptr = malloc(24);
	show_alloc_mem();
	return (0);
}
