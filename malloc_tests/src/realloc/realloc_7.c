#include "ft_malloc.h"

/*
 * prev, ptr + right
 */

int main()
{
	void *prev, *in_use, *new_ptr, *ptr;

	prev = malloc(0);
	in_use = malloc(1);
	ptr = malloc(12);
	new_ptr = malloc(2);
	in_use = malloc(3);
	show_alloc_mem();
	free(prev);
	free(new_ptr);
	show_alloc_mem();
	new_ptr = realloc(ptr, 29);
	show_alloc_mem();
	return (0);
}
