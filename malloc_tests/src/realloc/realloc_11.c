#include "ft_malloc.h"

/*
 * merge with part of left
 */

int main()
{
	void *prev, *in_use, *left, *right, *new_ptr, *ptr;

	prev = malloc(0);
	in_use = malloc(0);
	left = malloc(32);
	ptr = malloc(44);
	right = malloc(5);
	in_use = malloc(0);
	show_alloc_mem();
	free(prev);
	free(left);
	free(right);
	show_alloc_mem();
	new_ptr = realloc(ptr, 50);
	show_alloc_mem();
	in_use = malloc(0);
	in_use = malloc(0);
	in_use = malloc(0);
	show_alloc_mem();
	return (0);
}
