#include "ft_malloc.h"

/*
 * prev, left part + ptr
 */

int main()
{
	void *prev, *in_use, *new_ptr, *ptr;

	prev = malloc(0);
	in_use = malloc(2);
	new_ptr = malloc(4);
	ptr = malloc(13);
	in_use = malloc(6);
	show_alloc_mem();
	free(prev);
	free(new_ptr);
	show_alloc_mem();
	new_ptr = realloc(ptr, 29);
	show_alloc_mem();
	return (0);
}
