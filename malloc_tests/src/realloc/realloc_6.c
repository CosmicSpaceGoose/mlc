#include "ft_malloc.h"

/*
 * prev, ptr + right (duplicate)
 */

int main()
{
	void *prev, *in_use, *new_ptr, *ptr;

	prev = malloc(8);
	in_use = malloc(8);
	ptr = malloc(16);
	new_ptr = malloc(8);
	in_use = malloc(8);
	show_alloc_mem();
	free(prev);
	free(new_ptr);
	show_alloc_mem();
	new_ptr = realloc(ptr, 24);
	show_alloc_mem();
	return (0);
}
