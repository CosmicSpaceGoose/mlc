#include "ft_malloc.h"

/*
 *	merge with part of right
 */

int main()
{
	void *prev, *in_use, *left, *right, *ptr, *new_ptr;

	prev = malloc(8);
	in_use = malloc(8);
	left = malloc(8);
	ptr = malloc(8);
	right = malloc(32);
	in_use = malloc(0);
	show_alloc_mem();
	free(prev);
	free(left);
	free(right);
	show_alloc_mem();
	new_ptr = realloc(ptr, 16);
	show_alloc_mem();
	in_use = malloc(0);
	in_use = malloc(0);
	in_use = malloc(0);
	show_alloc_mem();
	return (0);
}
