#include "ft_malloc.h"
#include <string.h>
#include <stdio.h>

/*
 * realloc tiny 2 tiny chunk
 */
int main(void)
{
	char *ptr, *new_ptr, *in_use;

	ptr = malloc(8);
	strcpy(ptr, "ABCDEFG");
	printf("%p:%s\n", ptr, ptr);
	new_ptr = realloc(ptr, 16);
	show_alloc_mem();
	strncat(new_ptr, "QWERTYU", 8);
	printf("%p:%s\n", new_ptr, new_ptr);
	in_use = malloc(8);
	in_use = malloc(8);
	show_alloc_mem();
	return (0);
}
