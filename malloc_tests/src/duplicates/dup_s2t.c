#include "ft_malloc.h"
#include <string.h>
#include <stdio.h>

/*
 * realloc small 2 tiny chunk
 */
int main()
{
	char *ptr = malloc(1000);
	char *in_use = malloc(500);
	strcpy(ptr, "ptr, contain text... 12345678 12345678 12345678 12345678\
12345678 12345678 12345678 12345678 12345678....");
	printf("%p:%s\n", ptr, ptr);
	char *new_ptr = realloc(ptr, 20);
	new_ptr[19] = 0;
	printf("%p:%s\n", new_ptr, new_ptr);
	in_use = malloc(1000);
	show_alloc_mem();
	return (0);
}
