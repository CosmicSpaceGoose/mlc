#include "ft_malloc.h"
#include <string.h>
#include <stdio.h>

/*
 * realloc small 2 large chunk
 */
int main()
{
	char *ptr = malloc(1000);
	char *in_use = malloc(500);
	memset((void *)ptr, '#', 999);
	ptr[999] = 0;
	printf("%p:%s\n", ptr, ptr + 800);
	char *new_ptr = realloc(ptr, 5000);
	strcat(new_ptr, "||| aaaaaand... realloc and add new text 12345678 12345678\
12345678 12345678 12345678 12345678 12345678 12345678 12345678");
	printf("%p:%s\n", new_ptr, new_ptr + 800);
	in_use = malloc(1000);
	show_alloc_mem();
	return (0);
}
