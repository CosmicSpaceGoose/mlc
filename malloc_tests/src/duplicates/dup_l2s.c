#include "ft_malloc.h"
#include <string.h>
#include <stdio.h>

/*
 * realloc large 2 small chunk
 */
int main()
{
	char *ptr = malloc(5000);
	memset(ptr, '!', 5000);
	ptr[4999] = 0;
	printf("%p:%s\n", ptr, ptr + 4800);
	show_alloc_mem();
	char *new_ptr = realloc(ptr, 1000);
	new_ptr[999] = 0;
	printf("%p:%s\n", new_ptr, new_ptr + 800);
	show_alloc_mem();
	return (0);
}
