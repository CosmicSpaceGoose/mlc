#include "ft_malloc.h"
#include <string.h>
#include <stdio.h>

/*
 * realloc tiny 2 large chunk
 */
int main(void)
{
	char *ptr, *new_ptr, *in_use;

	ptr = malloc(16);
	strcpy(ptr, "ABCDEFG123456789");
	printf("%p:%s\n", ptr, ptr);
	show_alloc_mem();
	new_ptr = realloc(ptr, 5000);
	strcat(new_ptr, "QWERTYU");
	printf("%p:%s\n", new_ptr, new_ptr);
	in_use = malloc(16);
	in_use = malloc(150);
	show_alloc_mem();
	return (0);
}
