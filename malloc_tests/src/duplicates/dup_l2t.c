#include "ft_malloc.h"
#include <string.h>
#include <stdio.h>
#include <unistd.h>
/*
 * realloc large 2 tiny chunk
 */
int main()
{
	char *ptr = malloc(5000);
	memset(ptr, '-', 5000);
	ptr[4999] = 0;
	printf("%p:%s\n", ptr, ptr + 4800);
	show_alloc_mem();
	char *new_ptr = realloc(ptr, 100);
	new_ptr[99] = 0;
	printf("%p:%s:%zu\n", new_ptr, new_ptr, strlen(new_ptr));
	show_alloc_mem();
	return (0);
}
