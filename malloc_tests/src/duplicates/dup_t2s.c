#include "ft_malloc.h"
#include <string.h>
#include <stdio.h>

/*
 * realloc tiny 2 small chunk
 */
int main(void)
{
	char *ptr, *new_ptr, *in_use;

	ptr = malloc(8);
	strncpy(ptr, "ABCDEFG\0", 8);
	printf("%p:%s\n", ptr, ptr);
	new_ptr = realloc(ptr, 160);
	strncat(new_ptr, "QWERTYU", 8);
	printf("%p:%s\n", new_ptr, new_ptr);
	in_use = malloc(8);
	in_use = malloc(150);
	show_alloc_mem();
	return (0);
}
