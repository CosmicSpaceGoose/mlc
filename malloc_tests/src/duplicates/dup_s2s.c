#include "ft_malloc.h"
#include <string.h>
#include <stdio.h>

/*
 * realloc small 2 small chunk
 */
int main()
{
	char *ptr = malloc(1000);
	strcpy(ptr + 100, "ptr, contain text... 12345678 12345678 12345678 12345678\
 12345678 12345678 12345678 12345678 12345678....");
	printf("%p:%s\n", ptr, ptr + 100);
	show_alloc_mem();
	char *new_ptr = realloc(ptr, 2000);
	strcat(new_ptr + 100, "||| aaaaaand... realloc and add new text 12345678 12345678\
 12345678 12345678 12345678 12345678 12345678 12345678 12345678");
	printf("%p:%s\n", new_ptr, new_ptr + 100);
	show_alloc_mem();
	return (0);
}
