#include "ft_malloc.h"
#include <string.h>
#include <stdio.h>

/*
 * realloc large 2 large chunk
 */
int main()
{
	char *ptr = malloc(5000);
	memset(ptr, '*', 4988);
	ptr[4988] = 0;
	strcat(ptr, "SAFETY_TEXT");
	printf("%p:%s:%zu\n", ptr, ptr + 4900, strlen(ptr));
	show_alloc_mem();
	char *new_ptr = realloc(ptr, 10000);
	// strcat(new_ptr, "|||------------------------------");
	printf("%p:%s:%zu\n", new_ptr, new_ptr + 4900, strlen(new_ptr));
	show_alloc_mem();
	return (0);
}
