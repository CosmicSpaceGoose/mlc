#include "ft_malloc.h"

/*
 * merge with part of right and left
 */

int main()
{
	void *in_use, *left, *right, *new_ptr, *ptr;

	left = malloc(200);
	ptr = malloc(300);
	right = malloc(400);
	new_ptr = malloc(500);
	in_use = malloc(200);
	show_alloc_mem();
	free(left);
	free(right);
	show_alloc_mem();
	free(ptr);
	show_alloc_mem();
	free(new_ptr);
	show_alloc_mem();
	return (0);
}
