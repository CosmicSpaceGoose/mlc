#include "ft_malloc.h"

int	main(void)
{
	void	*a;
	int		*b;
	size_t	*c;

	a = malloc(800);
	b = (int *)malloc(sizeof(int) * 200);
	c = (size_t *)malloc(sizeof(size_t) * 100);
	show_alloc_mem();
	return (0);
}
