#include "ft_malloc.h"

int main(void)
{
	char *b, *c, *x;

	x = malloc(300);
	x = malloc(400);
	c = malloc(500);
	b = malloc(600);
	show_alloc_mem();
	free(b);
	free(c);
	show_alloc_mem();
	c = malloc(1108);
	show_alloc_mem();
	return 0;
}
