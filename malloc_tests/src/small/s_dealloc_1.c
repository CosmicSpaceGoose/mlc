#include "ft_malloc.h"

int main(void)
{
	void *b, *x;

	x = malloc(200);
	b = malloc(200);
	x = malloc(200);
	show_alloc_mem();
	free(b);
	b = malloc(200);
	show_alloc_mem();
	return (0);
}
