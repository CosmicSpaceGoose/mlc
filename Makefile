# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/01 16:00:06 by dlinkin           #+#    #+#              #
#    Updated: 2019/09/14 14:07:43 by dlinkin          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

OS	:=	$(shell uname)
ifeq ($(OS),Darwin)
	CYAN	:=	\x1b[1;36m
	NON		:=	\x1b[0m
	CYANN	:=	\x1b[36m
	GREEN	:=	\x1b[32m
endif
STRING1 = $(CYAN)---Compile_$(NAME)$(NON)
STRING2 = $(CYAN)---Remove_$(NAME)_O_Files$(NON)
STRING3 = $(CYAN)---Remove_$(NAME)$(NON)
################################################################################
ifeq ($(HOSTTYPE),)
	HOSTTYPE	:=	$(shell uname -m)_$(shell uname -s)
endif
NAME			:=	libft_malloc_$(HOSTTYPE).so
PROJECTDIR		:=	$(shell pwd -P)
LINK			:=	libft_malloc.so
DIRSRC			:=	./src/
DIROBJ			:=	./obj/
HDR				:=	./inc/ft_m_inner.h\
					./inc/ft_malloc.h
SRC				:=	ft_free.c\
					ft_m_allocations.c\
					ft_m_debug.c\
					ft_m_global_symbols.c\
					ft_m_info.c\
					ft_malloc.c\
					ft_printer.c \
					ft_re_large_chunks.c\
					ft_re_smaller_chunks.c\
					ft_realloc.c\
					ft_show_alloc_mem.c\
					ft_show_hex.c
OBJ				:=	$(addprefix $(DIROBJ), $(SRC:.c=.o))
CC				:=	gcc
CFLAGS			:=	-Wall -Wextra -Werror -fpic -g -fno-common
SOFLAGS			:=	-shared
INC				:=	-I./inc/
################################################################################

.PHONY: all clean re #$(HDR)

all: $(NAME)

$(NAME): $(DIROBJ) $(OBJ)
	$(CC) $(SOFLAGS) -o $(NAME) $(OBJ)
	ln -sf $(PROJECTDIR)/$(NAME) $(LINK)
	@echo "$(STRING1)"

$(DIROBJ):
	@mkdir -p $(DIROBJ)

$(DIROBJ)%.o: $(DIRSRC)%.c $(HDR)
	@$(CC) $(INC) $(CFLAGS) -o $@ -c $<
	@echo "$(CYANN)comp$(NON)..."$@

echo:
	@echo $(OBJ)
	@echo $(SRC)
	@echo $(PROJECTDIR)/$(LINK)

clean:
	@rm -rf $(DIROBJ)
	@echo "$(STRING2)"

fclean: clean
	@rm -rf $(NAME) $(LINK)
	@echo "$(STRING3)"

re: fclean all

dbg: all
	gcc -g -I./inc $(PROJECTDIR)/$(NAME) main.c -o re-debug
	# gcc -g -I./inc $(PROJECTDIR)/$(NAME) ./malloc_tests/src/tiny/t_dealloc_2.c -o re-debug
	./re-debug

tester:
	./malloc_tests/malloc_test.sh

backup:
	git add .
	git commit -m "BACKUP-`date '+%d-%m-%H:%M'`"
	git push

commit:
	git add .
	git commit -m "$(MSG)"
	git push
