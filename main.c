#include <string.h>
#include "ft_malloc.h"

int main()
{
	// char *b = malloc(40);
	// strcpy(b, "I'l copy sum text 'ere");
	// show_alloc_mem();
	char *b, *c, *x;

	x = malloc(4);
	x = malloc(4);
	c = malloc(4);
	b = malloc(4);
	show_alloc_mem();
	free(b);
	free(c);
	show_alloc_mem();
	c = malloc(24);
	strcpy(c, "1234567890");
	show_alloc_mem();
	return (0);
}
