/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/01 16:01:47 by dlinkin           #+#    #+#             */
/*   Updated: 2019/09/14 14:00:25 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_m_inner.h"

static void	merge_with_neighbors(t_zone *zone, t_free *chunk, t_free *left,
									t_free *right)
{
	if (right && (void *)chunk + chunk->size == (void *)right)
	{
		chunk->size += right->size;
		chunk->next = right->next;
	}
	else
		chunk->next = right;
	if (left && (void *)left + left->size == (void *)chunk)
	{
		left->size += chunk->size;
		left->next = chunk->next;
	}
	else if (left)
		left->next = chunk;
	else
		zone->free_ptr = chunk;
}

void		fr_inner(t_zone *zone, t_chunk *chunk)
{
	void *right;
	void *left;

	left = NULL;
	right = zone->free_ptr;
	zone->free += chunk->size;
	zone->in_use -= chunk->size;
	chunk->bits -= MLC_IN_USE;
	while (right && (void *)right < (void *)chunk)
	{
		left = right;
		right = ((t_free *)right)->next;
	}
	merge_with_neighbors(zone, (t_free *)chunk, left, right);
	if (g__.dbg_sym & MLC_ECHO)
		echo_massage("free", chunk + 1, chunk->size);
}

void		fr_large(void *ptr)
{
	t_large	**large;
	t_large	*copy;

	large = &g__.lrg;
	while (*large)
	{
		if (*large + 1 == (t_large *)ptr)
		{
			copy = *large;
			*large = (*large)->next;
			if (g__.dbg_sym & MLC_ECHO)
				echo_massage("free", copy, copy->size);
			if (munmap(copy, copy->size) == -1)
				malloc_error("munmap error", *large);
			return ;
		}
		large = &(*large)->next;
	}
	malloc_error("pointer being free'd wasn't allocated", ptr);
}

static int	check_inner(t_chunk *chunk)
{
	t_heap **heap;

	heap = &g__.heap;
	while (*heap)
	{
		if ((void *)chunk > (void *)*heap
			&& (void *)chunk < (void *)*heap + g__.units.heap_size)
		{
			chunk--;
			if (chunk->bits == (MLC_IN_USE | MLC_TINY))
				fr_inner((*heap)->tny, chunk);
			else if (chunk->bits == (MLC_IN_USE | MLC_SMALL))
				fr_inner((*heap)->sml, chunk);
			else
				malloc_error("pointer being free'd wasn't allocated",
				chunk + 1);
			if ((*heap)->tny->in_use == 0 && (*heap)->sml->in_use == 0)
				return_heap(heap);
			return (1);
		}
		heap = &(*heap)->nxt;
	}
	return (0);
}

void		free(void *ptr)
{
	if (!ptr)
	{
		if (g__.dbg_sym & MLC_ECHO)
			echo_massage("free", ptr, 0);
		return ;
	}
	if (!g__.heap || (size_t)ptr % 8)
		malloc_error("pointer being free'd wasn't allocated", ptr);
	if (!check_inner(ptr))
		fr_large(ptr);
}
