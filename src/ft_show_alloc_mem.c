/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show_alloc_mem.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/01 16:01:54 by dlinkin           #+#    #+#             */
/*   Updated: 2019/09/14 16:18:02 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_m_inner.h"

static void	show_large_zones(t_large *large, size_t *total)
{
	while (large)
	{
		print_formated(g__.dbg_fd, "%s%x\n\t%s%x%s%x%s%s%s%d%s%s%s%x\n",
						"LARGE\t", large, g__.col[5],
						(g__.dbg_sym & MLC_SHOW_CHUNK ? large : large + 1),
						" - ", (void *)large + large->size, g__.col[0],
						" : ", g__.col[1],
						large->size - (g__.dbg_sym & MLC_SHOW_CHUNK ? 0 :
															sizeof(t_large)),
						" bytes", g__.col[0],
						"\t-> ", ((t_large *)large)->next);
		if (g__.dbg_sym & MLC_SHOW_HEX_DUMP)
		{
			print_mem((void *)large, 128);
			write(g__.dbg_fd, "...\n", 4);
		}
		*total += (large->size - sizeof(t_large));
		total[2] += sizeof(t_large);
		total[3]++;
		large = large->next;
	}
}

static void	print_chunk(t_chunk *chunk, int mod)
{
	print_formated(g__.dbg_fd, "\t%s%x%s%x%s%s%s%d%s%s",
				(mod ? g__.col[6] : g__.col[5]),
				(g__.dbg_sym & MLC_SHOW_CHUNK ? chunk : chunk + 1),
				" - ", (void *)chunk + chunk->size, g__.col[0],
				" : ", g__.col[1],
				chunk->size - (g__.dbg_sym & MLC_SHOW_CHUNK ? 0 :
														sizeof(t_chunk)),
				" bytes", g__.col[0]);
	if (mod)
		print_formated(g__.dbg_fd, "\t-> %x\n", ((t_free *)chunk)->next);
	else
	{
		write(g__.dbg_fd, "\n", 1);
	}
	if (g__.dbg_sym & MLC_SHOW_HEX_DUMP)
	{
		if (chunk->size > 64)
		{
			print_mem((void *)chunk, 64);
			write(g__.dbg_fd, "...\n", 4);
		}
		else
			print_mem((void *)chunk, chunk->size);
	}
}

static void	show_smaller_zone(t_chunk *heap, size_t *total, void *end,
								char *name)
{
	print_formated(g__.dbg_fd, "%s\t%x\n", name, (size_t)heap);
	if (g__.dbg_sym & MLC_SHOW_FULL_INFO)
		print_formated(g__.dbg_fd,
						"\t%sin use - %d bytes\n\tfree - %d bytes%s\n",
						g__.col[1], ((t_zone *)heap)->in_use,
						((t_zone *)heap)->free, g__.col[0]);
	heap = (void *)heap + sizeof(t_zone);
	total[2] += sizeof(t_zone);
	while ((void *)heap < end)
	{
		if (heap->bits & 1)
		{
			print_chunk(heap, 0);
			total[0] += (heap->size - sizeof(t_chunk));
			total[3]++;
		}
		else
		{
			if (g__.dbg_sym & MLC_SHOW_EMPTY)
				print_chunk(heap, 1);
			total[1] += (heap->size - sizeof(t_chunk));
		}
		total[2] += sizeof(t_chunk);
		heap = (void *)heap + heap->size;
	}
}

static void	show_loop(t_heap *heap, size_t *total)
{
	while (heap)
	{
		total[2] += sizeof(t_heap);
		print_formated(g__.dbg_fd, "%s%s%x%s\n",
				g__.col[2], "== PARTITION => ", (size_t)heap, g__.col[0]);
		show_smaller_zone((t_chunk *)heap->tny, total,
							(void *)heap->tny + g__.units.size_tny, "TINY");
		show_smaller_zone((t_chunk *)heap->sml, total,
							(void *)heap->sml + g__.units.size_sml, "SMALL");
		heap = heap->nxt;
	}
	show_large_zones(g__.lrg, total);
}

void		show_alloc_mem(void)
{
	size_t	total[4];

	if (!g__.heap)
	{
		write(2, "42MLC: Memory not allocated\n", 28);
		return ;
	}
	total[0] = 0;
	total[1] = 0;
	total[2] = 0;
	total[3] = 0;
	show_loop(g__.heap, total);
	if (g__.dbg_sym & MLC_SHOW_TOTAL)
	{
		print_formated(g__.dbg_fd, "%s%s%d%s%d%s%d%s%d%s%s", g__.col[1],
						"======= TOTAL =======\nallocated - ", total[0],
						" bytes (in ", total[3],
						" chunks)\nfree - ", total[1],
						" bytes\nmetadata - ", total[2],
						" bytes\n", g__.col[0]);
	}
}
