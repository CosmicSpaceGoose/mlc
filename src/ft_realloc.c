/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/01 16:01:52 by dlinkin           #+#    #+#             */
/*   Updated: 2019/09/14 16:17:31 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_m_inner.h"

/*
** duplicate smaller chunk
*/

static void	*dup_chunk(t_zone *zone, t_chunk *chunk, size_t new_size)
{
	void	*new;
	size_t	shift;

	new = alloc_chunk(g__.heap, new_size);
	if (new)
	{
		if (new_size > chunk->size)
			shift = chunk->size / sizeof(t_chunk);
		else
			shift = new_size / sizeof(t_chunk);
		duplicate_chunk(chunk + 1, new, chunk + shift);
		fr_inner(zone, chunk);
	}
	return (new);
}

/*
** if new size < chunk size return chunk with reduced size
** || try merge with free neighbours chunk
** || allocate new chunk
*/

static void	*try_chop_chunk(struct s_zone *zone, struct s_chunk *chunk,
							size_t new_size)
{
	struct s_chunk	*choped_chunk;

	if (new_size + sizeof(struct s_free) <= chunk->size)
	{
		choped_chunk = chunk + new_size / sizeof(struct s_chunk);
		choped_chunk->bits = chunk->bits;
		choped_chunk->size = chunk->size - new_size;
		chunk->size = new_size;
		fr_inner(zone, choped_chunk);
	}
	else if (!re_smaller(zone, &chunk, new_size))
		return (dup_chunk(zone, chunk, new_size));
	return ((void *)(chunk + 1));
}

/*
** check tiny OR small memory zones
*/

static void	*search_zone(t_heap *heap, t_chunk *chunk, size_t new_size)
{
	if (chunk->bits != (MLC_IN_USE | MLC_TINY)
	&& chunk->bits != (MLC_IN_USE | MLC_SMALL))
		malloc_error("pointer being realloc'd wasn't allocated",
					(void *)(chunk + 1));
	new_size += sizeof(t_chunk);
	if (new_size == chunk->size)
		return ((void *)(chunk + 1));
	if (chunk->bits & 2)
	{
		if (new_size > g__.units.limit_tny)
			return (dup_chunk(heap->tny, chunk, new_size));
		else
			return (try_chop_chunk(heap->tny, chunk, new_size));
	}
	if (new_size <= g__.units.limit_tny || new_size > g__.units.limit_sml)
		return (dup_chunk(heap->sml, chunk, new_size));
	return (try_chop_chunk(heap->sml, chunk, new_size));
}

static void	*realloc_wraper(t_heap **heap, void *ptr, size_t size)
{
	ptr = search_zone(*heap, (t_chunk *)ptr - 1, size);
	if ((*heap)->tny->in_use == 0 && (*heap)->sml->in_use == 0)
		return_heap(heap);
	if (g__.dbg_sym & MLC_ECHO)
		echo_massage("realloc", ptr, size);
	return (ptr);
}

void		*realloc(void *ptr, size_t size)
{
	t_heap **heap;

	if (!ptr || (ptr && !size))
	{
		if (ptr && !size)
			free(ptr);
		return (malloc(size));
	}
	if (!g__.heap || (size_t)ptr & 7)
		malloc_error("pointer being realloc'd wasn't allocated", ptr);
	if (size % sizeof(t_chunk))
		size = (size / sizeof(t_chunk) + 1) * sizeof(t_chunk);
	heap = &g__.heap;
	while (*heap)
	{
		if (ptr > (void *)((*heap)->tny)
		&& ptr < (void *)*heap + g__.units.heap_size)
			return (realloc_wraper(heap, ptr, size));
		heap = &(*heap)->nxt;
	}
	return (re_larger(g__.lrg, ptr, size));
}
