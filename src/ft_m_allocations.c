/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_m_allocations.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 16:10:32 by dlinkin           #+#    #+#             */
/*   Updated: 2019/09/14 15:42:37 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_m_inner.h"

/*
** 'allocates' chunk from first free chunk, 'return' ptr
*/

static void	get_this_chunk(t_free **chunk, size_t size, size_t bits)
{
	t_free *free_ptr;

	free_ptr = *chunk;
	if (free_ptr->size > size)
	{
		free_ptr = (void *)free_ptr + size;
		free_ptr->bits = (*chunk)->bits;
		free_ptr->size = (*chunk)->size - size;
		free_ptr->next = (*chunk)->next;
	}
	else
		free_ptr = (*chunk)->next;
	(*chunk)->bits = bits;
	(*chunk)->size = size;
	*chunk = free_ptr;
}

/*
** search 'compatible' free chunk in zone
*/

static int	alloc_smaller(void **ptr, t_zone *zone, size_t size, size_t bits)
{
	t_free	**free_ptr;

	free_ptr = &zone->free_ptr;
	while (free_ptr)
	{
		if ((*free_ptr)->size == size
			|| size + sizeof(t_free) <= (*free_ptr)->size)
		{
			zone->in_use += size;
			zone->free -= size;
			*ptr = (t_chunk *)*free_ptr + 1;
			get_this_chunk(free_ptr, size, bits);
			return (1);
		}
		free_ptr = &(*free_ptr)->next;
	}
	return (0);
}

/*
** mmap large chunk
*/

static void	*alloc_large(size_t size)
{
	t_large	*large;
	t_large	**left;
	t_large	*right;

	size = size + sizeof(t_large) - sizeof(t_chunk);
	if (size % g__.units.page_size)
		size = (size / g__.units.page_size + 1) * g__.units.page_size;
	large = get_new_zone(MLC_START_ADDR, size);
	if (large)
	{
		left = &g__.lrg;
		right = g__.lrg;
		while (right && right < large)
		{
			left = &right->next;
			right = right->next;
		}
		*left = large;
		large->next = right;
		large->size = size;
		return (large + 1);
	}
	return (NULL);
}

static int	allocate_in_exist_heap(t_heap *heap, void **ptr, size_t size)
{
	if (size - sizeof(t_chunk) <= g__.units.limit_tny)
	{
		if (heap->tny->free >= size
		&& alloc_smaller(ptr, heap->tny, size, MLC_IN_USE | MLC_TINY))
			return (1);
	}
	else if (size - sizeof(t_chunk) <= g__.units.limit_sml)
	{
		if (heap->sml->free >= size
		&& alloc_smaller(ptr, heap->sml, size, MLC_IN_USE | MLC_SMALL))
			return (1);
	}
	else
	{
		*ptr = alloc_large(size);
		return (1);
	}
	return (0);
}

void		*alloc_chunk(t_heap *heap, size_t size)
{
	void *ptr;

	ptr = NULL;
	while (heap)
	{
		if (allocate_in_exist_heap(heap, &ptr, size))
			break ;
		if (!heap->nxt)
		{
			heap->nxt = get_new_zone(heap, g__.units.heap_size);
			if (!heap->nxt)
				break ;
			init_heap(heap->nxt);
		}
		heap = heap->nxt;
	}
	return (ptr);
}
