/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_m_info.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/22 17:51:52 by dlinkin           #+#    #+#             */
/*   Updated: 2018/10/22 17:51:54 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_m_inner.h"

void		malloc_error(char *msg, void *ptr)
{
	if (ptr)
		print_formated(2, "42MLC: %s (%x)\n", msg, ptr);
	else
		print_formated(2, "42MLC: %s\n", msg);
	kill(getpid(), SIGABRT);
}

void		echo_massage(char *who, void *addr, size_t size)
{
	if (g__.dbg_sym & MLC_ECHO_TIMESTAMP)
		print_formated(g__.dbg_fd,
						"%s-pid[%d]:time[%d]%x - %d bytes\n",
						who, getpid(), time(NULL), addr, size);
	else
		print_formated(g__.dbg_fd,
						"%s-pid[%d]:%x - %d bytes\n",
						who, getpid(), addr, size);
}
