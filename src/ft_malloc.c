/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/01 16:01:50 by dlinkin           #+#    #+#             */
/*   Updated: 2019/09/14 16:16:48 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_m_inner.h"

void		init_heap(t_heap *heap)
{
	t_free *chunk;

	heap->tny = (void *)(heap + 1);
	chunk = (void *)heap->tny + sizeof(t_zone);
	chunk->bits = MLC_TINY;
	chunk->size = g__.units.size_tny - sizeof(t_zone);
	chunk->next = NULL;
	heap->tny->free_ptr = chunk;
	heap->tny->in_use = 0;
	heap->tny->free = chunk->size;
	heap->sml = (void *)heap->tny + g__.units.size_tny;
	chunk = (void *)heap->sml + sizeof(t_zone);
	chunk->bits = MLC_SMALL;
	chunk->size = g__.units.size_sml - sizeof(t_zone);
	chunk->next = NULL;
	heap->sml->free_ptr = chunk;
	heap->sml->in_use = 0;
	heap->sml->free = chunk->size;
	heap->nxt = NULL;
	if (g__.dbg_sym & MLC_ECHO)
		echo_massage("malloc:init_heap", heap, g__.units.heap_size);
}

void		return_heap(t_heap **heap)
{
	t_heap *copy;

	copy = *heap;
	if (g__.dbg_sym & MLC_ECHO)
		echo_massage("malloc:return_heap", copy, g__.units.heap_size);
	*heap = (*heap)->nxt;
	if (munmap(copy, g__.units.heap_size) == -1)
		malloc_error("munmap error", copy);
}

void		*get_new_zone(void *addr, size_t size)
{
	void *ptr;

	ptr = mmap(addr, size, PROT_READ | PROT_WRITE,
				MAP_ANON | MAP_PRIVATE, -1, 0);
	if (ptr == MAP_FAILED)
	{
		if (errno != ENOMEM)
			malloc_error("mmap failed", NULL);
		if (g__.dbg_sym & MLC_ECHO)
			echo_massage("malloc", NULL, 0);
		return (NULL);
	}
	return (ptr);
}

static void	init_units(void)
{
	size_t		pagesize;
	size_t		num_pages;

	if (!(pagesize = (size_t)getpagesize()))
		malloc_error("PAGESIZE is zero", NULL);
	g__.units.limit_tny = MLC_LIMIT_TINY - sizeof(t_chunk);
	g__.units.limit_sml = pagesize - sizeof(t_chunk);
	num_pages = (MLC_LIMIT_TINY * MLC_TINY_COUNT) / pagesize + 1;
	g__.units.size_tny = pagesize * num_pages - sizeof(t_zone);
	g__.units.size_sml = pagesize * MLC_SMALL_COUNT + sizeof(t_zone);
	g__.units.heap_size = g__.units.size_sml + g__.units.size_tny;
	g__.units.size_tny -= sizeof(t_heap);
	g__.units.page_size = pagesize;
	if (g__.dbg_sym & MLC_ECHO_UNITS)
	{
		print_formated(g__.dbg_fd, "%s%d%s%d%s%d%s%d%s%d%s%d%s",
			"MLC_UNITS:\nPartition - ", g__.units.heap_size,
			" bytes\nMaximum tiny block - ", g__.units.limit_tny,
			" bytes\nMaximum small block - ", g__.units.limit_sml,
			" bytes\nTiny zone - ", g__.units.size_tny,
			" bytes\nSmall zone - ", g__.units.size_sml,
			" bytes\nPagesize - ", g__.units.page_size, " bytes\n");
	}
}

void		*malloc(size_t size)
{
	static int	was_initialized;
	void		*ptr;

	if (!was_initialized)
	{
		init_debug_symbols();
		init_units();
		was_initialized = 1;
	}
	if (!g__.heap)
	{
		g__.heap = get_new_zone(MLC_START_ADDR, g__.units.heap_size);
		if (!g__.heap)
			return (NULL);
		init_heap((t_heap *)g__.heap);
	}
	if (!size)
		size = sizeof(t_chunk);
	if (size % sizeof(t_chunk))
		size = (size / sizeof(t_chunk) + 1) * sizeof(t_chunk);
	ptr = alloc_chunk(g__.heap, size + sizeof(t_chunk));
	if (g__.dbg_sym & MLC_ECHO)
		echo_massage("malloc", ptr, size);
	return (ptr);
}
