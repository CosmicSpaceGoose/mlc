/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_re_large_chunks.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 13:30:30 by dlinkin           #+#    #+#             */
/*   Updated: 2019/09/14 15:44:51 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_m_inner.h"

void		duplicate_chunk(t_chunk *from, t_chunk *to, t_chunk *end)
{
	while (from < end)
	{
		*to = *from;
		to++;
		from++;
	}
}

static void	*large_chunk(t_large *large, void *ptr, size_t size,
							size_t ch_size)
{
	void	*new_ptr;

	new_ptr = alloc_chunk(g__.heap, size + sizeof(t_chunk));
	if (!new_ptr)
		return (NULL);
	if (large->size > ch_size)
		duplicate_chunk(ptr, new_ptr, ptr + size);
	else
		duplicate_chunk(ptr, new_ptr, ptr + large->size - sizeof(t_large));
	if (munmap(large, large->size) == -1)
		malloc_error("munmap error", NULL);
	if (g__.dbg_sym & MLC_ECHO)
		echo_massage("realloc", new_ptr, size);
	return (new_ptr);
}

void		*re_larger(t_large *large, void *ptr, size_t size)
{
	t_large	**prev;
	size_t	ch_size;

	prev = &g__.lrg;
	print_formated(2, "new %d\n", size);
	while (large)
	{
		if (large + 1 == (t_large *)ptr)
		{
			ch_size = size + sizeof(t_large *);
			if (ch_size % g__.units.page_size)
				ch_size = (ch_size / g__.units.page_size + 1) *
														g__.units.page_size;
			if (large->size == ch_size)
				return (ptr);
			*prev = large->next;
			return (large_chunk(large, ptr, size, ch_size));
		}
		prev = &large->next;
		large = large->next;
	}
	malloc_error("pointer being realloc'd wasn't allocated", ptr);
	return (NULL);
}
