/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_m_debug.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/08 15:27:54 by dlinkin           #+#    #+#             */
/*   Updated: 2019/09/14 14:04:46 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_m_inner.h"

static int	compare_func(char *env_variable, char *debug_symbol)
{
	while (*env_variable && *debug_symbol && *env_variable == *debug_symbol)
	{
		env_variable++;
		debug_symbol++;
	}
	return (*env_variable == '=');
}

static void	mlc_echo_init(char *variable)
{
	char	*ptr;

	g__.dbg_sym |= MLC_ECHO;
	while (*variable != '=')
		variable++;
	ptr = variable + 1;
	if (!*ptr)
		return ;
	g__.dbg_fd = 0;
	while (*ptr && *ptr >= '0' && *ptr <= '9')
	{
		g__.dbg_fd = g__.dbg_fd * 10 + (*ptr - 48);
		ptr++;
	}
}

static void	mlc_color_init(void)
{
	if (!isatty(g__.dbg_fd))
		return ;
	g__.col[0] = C_NON;
	g__.col[1] = C_HLF;
	g__.col[2] = C_REV;
	g__.col[3] = C_RED;
	g__.col[4] = C_GRN;
	g__.col[5] = C_YLW;
	g__.col[6] = C_BLU;
}

static void	read_debug_symbol(char *copy)
{
	if (compare_func(copy, "MLC_ECHO"))
		mlc_echo_init(copy);
	else if (compare_func(copy, "MLC_ECHO_UNITS"))
		g__.dbg_sym |= MLC_ECHO_UNITS;
	else if (compare_func(copy, "MLC_ECHO_TIMESTAMP"))
		g__.dbg_sym |= MLC_ECHO_TIMESTAMP;
	else if (compare_func(copy, "MLC_SHOW_TOTAL"))
		g__.dbg_sym |= MLC_SHOW_TOTAL;
	else if (compare_func(copy, "MLC_SHOW_EMPTY"))
		g__.dbg_sym |= MLC_SHOW_EMPTY;
	else if (compare_func(copy, "MLC_SHOW_FULL_INFO"))
		g__.dbg_sym |= MLC_SHOW_FULL_INFO;
	else if (compare_func(copy, "MLC_SHOW_HEX_DUMP"))
		g__.dbg_sym |= MLC_SHOW_HEX_DUMP;
	else if (compare_func(copy, "MLC_SHOW_IN_COLOR"))
		mlc_color_init();
	else if (compare_func(copy, "MLC_SHOW_CHUNK"))
		g__.dbg_sym |= MLC_SHOW_CHUNK;
}

void		init_debug_symbols(void)
{
	extern char	**environ;
	char		**copy;

	copy = environ;
	while (*copy)
	{
		read_debug_symbol(*copy);
		copy++;
	}
}
