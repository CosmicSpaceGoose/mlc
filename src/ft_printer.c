/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printer.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 14:09:55 by dlinkin           #+#    #+#             */
/*   Updated: 2019/09/14 16:16:24 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_m_inner.h"

static void	print_size_t(int fd, size_t size, size_t base)
{
	char		buffer[20];
	const char	*str = "0123456789abcdef";
	size_t		cpy;
	size_t		len_cpy;
	size_t		len;

	len = (base == 16 ? 3 : 1);
	cpy = size;
	while (cpy > (base - 1))
	{
		len++;
		cpy /= base;
	}
	len_cpy = len;
	while (len)
	{
		len--;
		buffer[len] = str[size % base];
		size /= base;
	}
	if (base == 16)
		buffer[1] = 'x';
	write(fd, buffer, len_cpy);
}

static void	print_string(int fd, char *str)
{
	size_t len;

	len = 0;
	if (!str)
		return ;
	while (str[len])
		len++;
	write(fd, str, len);
}

static void	print_hex_dump(int fd, unsigned char c)
{
	char		buffer[2];
	const char	*str = "0123456789abcdef";

	buffer[1] = str[c & 15];
	buffer[0] = str[c / 16];
	write(fd, buffer, 2);
}

void		print_formated(int fd, const char *format, ...)
{
	va_list ap;

	va_start(ap, format);
	while (*format)
	{
		if (*format != '%')
			write(fd, format, 1);
		else
		{
			format++;
			if (*format == 's')
				print_string(fd, va_arg(ap, char *));
			else if (*format == 'x')
				print_size_t(fd, va_arg(ap, size_t), 16);
			else if (*format == 'd')
				print_size_t(fd, va_arg(ap, size_t), 10);
			else if (*format == 'h')
				print_hex_dump(fd, (unsigned char)va_arg(ap, size_t));
		}
		format++;
	}
	va_end(ap);
}
