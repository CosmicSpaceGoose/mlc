/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show_hex.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 18:16:24 by dlinkin           #+#    #+#             */
/*   Updated: 2018/12/17 18:16:27 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_m_inner.h"

static void	check_character(unsigned char c)
{
	if (c == 0)
		print_formated(g__.dbg_fd, "%s", g__.col[1]);
	else if (c >= '0' && c <= '9')
		print_formated(g__.dbg_fd, "%s", g__.col[3]);
	else if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
		print_formated(g__.dbg_fd, "%s", g__.col[4]);
	else if (c > 31 && c < 127)
		print_formated(g__.dbg_fd, "%s", g__.col[5]);
	else
		print_formated(g__.dbg_fd, "%s", g__.col[6]);
}

static void	print_character_representation(size_t i, size_t size
	, unsigned char **string, unsigned char *end)
{
	if (i == size && size & 15)
		write(g__.dbg_fd, "                      ", 22);
	while (*string <= end)
	{
		if (**string > 31 && **string < 127)
			write(g__.dbg_fd, *string, 1);
		else
			write(g__.dbg_fd, ".", 1);
		(*string)++;
	}
	write(g__.dbg_fd, "\n", 1);
}

void		print_mem(void *ptr, size_t size)
{
	unsigned char	*hex;
	unsigned char	*cha;
	size_t			i;

	hex = (unsigned char *)ptr;
	cha = hex;
	i = 1;
	while (i <= size)
	{
		check_character(*hex);
		print_formated(g__.dbg_fd, "%h%s", *hex, g__.col[0]);
		if ((i & 1) == 0)
		{
			write(g__.dbg_fd, " ", 1);
			if ((i & 3) == 0)
				write(g__.dbg_fd, " ", 1);
		}
		if ((i & 15) == 0 || i == size)
			print_character_representation(i, size, &cha, hex);
		i++;
		hex++;
	}
}
