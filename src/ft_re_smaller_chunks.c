/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_re_smaller_chunks.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/28 17:34:38 by dlinkin           #+#    #+#             */
/*   Updated: 2019/09/14 16:18:26 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_m_inner.h"

/*
** merge chunk with left and/or right free chunks
*/

static void	add_right(t_zone *zone, t_chunk **chunk, t_free **ch, size_t size)
{
	zone->in_use += (size - (*chunk)->size);
	zone->free -= (size - (*chunk)->size);
	if ((*chunk)->size + ch[0]->size == size)
		ch[2] = ch[0]->next;
	else
	{
		ch[2] = (t_free *)((void *)*chunk + size);
		ch[2]->next = ch[0]->next;
		ch[2]->bits = ch[0]->bits;
		ch[2]->size = ch[0]->size - size + (*chunk)->size;
	}
	(*chunk)->size = size;
	if (ch[1])
		ch[1]->next = ch[2];
	else
		zone->free_ptr = ch[2];
}

static void	add_left(t_zone *zone, t_chunk **chunk, t_free **ch, size_t size)
{
	zone->in_use += (size - (*chunk)->size);
	zone->free -= (size - (*chunk)->size);
	if ((*chunk)->size + ch[1]->size == size)
	{
		if (ch[2])
			ch[2]->next = *ch;
		else
			zone->free_ptr = *ch;
	}
	else
	{
		ch[0] = ch[1];
		ch[0]->size -= (size - (*chunk)->size);
		ch[1] = (void *)ch[1] + ch[1]->size;
	}
	duplicate_chunk(*chunk, (t_chunk *)ch[1], (void *)ch[1] + size);
	ch[1]->size = size;
	*chunk = (t_chunk *)ch[1];
}

static void	add_both(t_zone *zone, t_chunk **chunk, t_free **ch, size_t size)
{
	zone->in_use += (size - (*chunk)->size);
	zone->free -= (size - (*chunk)->size);
	if ((*chunk)->size + ch[0]->size + ch[1]->size == size)
	{
		if (ch[2])
			ch[2]->next = ch[0]->next;
		else
			zone->free_ptr = ch[0]->next;
	}
	else
	{
		ch[2] = ch[1];
		ch[2]->next = ch[0]->next;
		ch[1] = (void *)ch[1] +
					(ch[1]->size + (*chunk)->size + ch[0]->size - size);
		ch[2]->size = ((*chunk)->size + ch[0]->size + ch[2]->size) - size;
	}
	duplicate_chunk(*chunk, (t_chunk *)ch[1], (void *)*chunk + (*chunk)->size);
	ch[1]->size = size;
	*chunk = (t_chunk *)ch[1];
}

static void	find_neighbours(t_free **ch, t_free *free_ptr, t_free *chunk)
{
	ch[0] = free_ptr;
	ch[1] = NULL;
	ch[2] = NULL;
	while (ch[0] && ch[0] < chunk)
	{
		ch[2] = ch[1];
		ch[1] = ch[0];
		ch[0] = ch[0]->next;
	}
}

int			re_smaller(t_zone *zone, t_chunk **chunk, size_t size)
{
	t_free	*ch[3];

	find_neighbours(ch, zone->free_ptr, (t_free *)*chunk);
	if (*ch && (void *)*chunk + (*chunk)->size == *ch
	&& (ch[0]->size + (*chunk)->size == size
		|| ch[0]->size + (*chunk)->size >= size + sizeof(t_free)))
		add_right(zone, chunk, ch, size);
	else if (ch[1] && (void *)ch[1] + ch[1]->size == *chunk
	&& (ch[1]->size + (*chunk)->size == size
		|| ch[1]->size + (*chunk)->size >= size + sizeof(t_free)))
		add_left(zone, chunk, ch, size);
	else if (ch[1] && *ch && (void *)ch[1] + ch[1]->size == *chunk
	&& (void *)*chunk + (*chunk)->size == *ch
	&& (ch[0]->size + (*chunk)->size + ch[1]->size == size
		|| ch[0]->size + (*chunk)->size + ch[1]->size >=
												size + sizeof(t_free)))
		add_both(zone, chunk, ch, size);
	else
		return (0);
	return (1);
}
