/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/01 16:02:08 by dlinkin           #+#    #+#             */
/*   Updated: 2018/10/01 16:02:11 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MALLOC_H
# define FT_MALLOC_H

# include <stddef.h>

void	free(void *ptr);
void	*malloc(size_t size) __attribute__((__warn_unused_result__));
void	*realloc(void *ptr
	, size_t size) __attribute__((__warn_unused_result__));
void	show_alloc_mem(void);

#endif
