/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_m_inner.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/10 14:30:11 by dlinkin           #+#    #+#             */
/*   Updated: 2019/09/14 16:15:48 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_M_INNER_H
# define FT_M_INNER_H

# include <sys/mman.h>
# include <errno.h>
# include <signal.h>
# include <stdarg.h>
# include <time.h>
# include <unistd.h>

# if defined(__APPLE__) && defined(__MACH__)
#  define MLC_START_ADDR ((void *)0x700000000000)
# elif defined(__CYGWIN__)
#  define MLC_START_ADDR ((void *)0x70000000)
# endif
# define MLC_LIMIT_TINY (128)
# define MLC_TINY_COUNT (100)
# define MLC_SMALL_COUNT (100)
# define MLC_IN_USE (1 << 0)
# define MLC_TINY (1 << 1)
# define MLC_SMALL (1 << 2)

/*
** malloc debug symbols
*/
# define MLC_ECHO (1)
# define MLC_ECHO_TIMESTAMP (1 << 1)
# define MLC_ECHO_UNITS (1 << 2)
# define MLC_SHOW_TOTAL (1 << 3)
# define MLC_SHOW_EMPTY (1 << 4)
# define MLC_SHOW_FULL_INFO (1 << 5)
# define MLC_SHOW_HEX_DUMP (1 << 6)
# define MLC_SHOW_CHUNK (1 << 7)

/*
** escape colours
*/
# define C_NON "\e[0m"
# define C_HLF "\e[2m"
# define C_REV "\e[7m"
# define C_RED "\e[31m"
# define C_GRN "\e[32m"
# define C_YLW "\e[33m"
# define C_BLU "\e[34m"

/*
** malloc memory units
*/
struct				s_units
{
	size_t	limit_tny;
	size_t	limit_sml;
	size_t	size_tny;
	size_t	size_sml;
	size_t	heap_size;
	size_t	page_size;
};

/*
** used and free chunks info
*/
typedef struct		s_chunk
{
	size_t	bits : 8;
	size_t	size : 24;
}					t_chunk;

typedef struct		s_free
{
	size_t			bits : 8;
	size_t			size : 24;
	struct s_free	*next;
}					t_free;

/*
** zone info
*/
typedef struct		s_zone
{
	t_free			*free_ptr;
	size_t			in_use : 48;
	size_t			free : 48;
}					t_zone;

/*
** large chunk info
*/
typedef struct		s_large
{
	struct s_large	*next;
	size_t			size;
}					t_large;

/*
** heap info
*/
typedef struct		s_heap
{
	t_zone			*tny;
	t_zone			*sml;
	struct s_heap	*nxt;
	char			*pdd;
}					t_heap;

/*
** global variable structure
*/
typedef struct		s_globals
{
	struct s_units	units;
	t_heap			*heap;
	t_large			*lrg;
	size_t			dbg_sym;
	size_t			dbg_fd;
	char			*col[7];
}					t_globals;

extern t_globals	g__;
extern void			free(void *ptr);
extern void			*malloc(size_t size);

/*
** ft_free.c
*/
void				fr_inner(t_zone *zone, t_chunk *chunk);
void				fr_large(void *ptr);

/*
** ft_m_allocations.c
*/
void				*alloc_chunk(t_heap *heap, size_t size);

/*
** ft_m_debug.c
*/
void				init_debug_symbols(void);

/*
** ft_m_info.c
*/
void				malloc_error(char *msg, void *ptr);
void				echo_massage(char *who, void *addr, size_t size);

/*
** ft_malloc.c
*/
void				init_heap(t_heap *heap);
void				return_heap(t_heap **heap);
void				*get_new_zone(void *addr, size_t size);

/*
** ft_printer.c
*/
void				print_formated(int fd, const char *format, ...);

/*
** ft_re_large_chunks.c
*/
void				*re_larger(t_large *large, void *ptr, size_t size);

/*
** ft_re_smaller_chunks.c
*/
void				duplicate_chunk(t_chunk *from, t_chunk *to, t_chunk *end);
int					re_smaller(t_zone *zone, t_chunk **ptr, size_t size);

/*
** ft_show_hex.c
*/
void				print_mem(void *ptr, size_t size);

#endif
