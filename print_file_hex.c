/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memt1.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlinkin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/01 16:01:20 by dlinkin           #+#    #+#             */
/*   Updated: 2018/10/01 16:01:29 by dlinkin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

const size_t blk_size___ = 16;

void	print_trail_spaces(size_t size)
{
	size_t spaces;

	spaces = (blk_size___ - size % blk_size___) * 2;
	while (spaces)
	{
		write(1, " ", 1);
		spaces--;
	}
}

void	print_mem(void *ptr, size_t size)
{
	char *hex = (char *)ptr;
	char *cha = hex;
	size_t i = 1;
	size_t r = 0;
	while (i <= size)
	{
		if (*hex == 0)
			write(1, "\e[2;m", 5);
		else if (*hex >= '0' && *hex <= '9')
			write(1, "\e[1;31m", 7);
		else if ((*hex >= 'A' && *hex <= 'Z') || (*hex >= 'a' && *hex <= 'z'))
			write(1, "\e[1;32m", 7);
		else if (*hex > 31 && *hex < 127)
			write(1, "\e[1;33m", 7);
		else
			write(1, "\e[1;34m", 7);
		printf("%02hhx\e[0m", *hex);
		if (i % 2 == 0)
		{
			write(1, " ", 1);
			if (i % 4 == 0)
				write(1, " ", 1);
		}
		if (i % blk_size___ == 0 || i == size)
		{
			if (i == size && size % blk_size___)
				print_trail_spaces(size);
			while (r < i)
			{
				if (cha[r] > 31 && cha[r] < 127)
					write(1, cha + r, 1);
				else
					write(1, ".", 1);
				r++;
			}
			write(1, "\n", 1);
		}
		i++;
		hex++;
	}
}

#include "ft_malloc.h"
#include <string.h>
int main(int ac, char **av)
{
	setbuf(stdout, NULL);
	// int fd, i;
	// char buf[1024];
	//
	// if ((fd = open(av[1], O_RDONLY)) == -1)
	// {
	// 	perror("ERROR");
	// 	return (1);
	// }
	// while ((i = read(fd, buf, 1024)))
	// 	print_mem(buf, i);
	char *ptr = malloc(5000);
	memset(ptr, '-', 5000);
	*ptr = '0';
	ptr[97] = '3';
	ptr[98] = '2';
	ptr[99] = '1';
	ptr[4999] = 0;
	print_mem(ptr - 8, 112);
	show_alloc_mem();
	char *new_ptr = realloc(ptr, 100);
	new_ptr[99] = 0;
	print_mem(new_ptr - 8, 112);
	show_alloc_mem();
	return (0);
}
